"""First API, local access only"""
# SPDX-License-Identifier: MIT

import subprocess

import falcon
import hug
import logging
import config

log = logging.getLogger(__name__)


@hug.get('/', output=hug.output_format.html)
def my_endpoint():
    return "<html></html>"


@hug.get('/api')
def list_applications():
    return map(lambda x: x.get("name"), config.APPLICATIONS)


@hug.get('/api/{app}')
def list_single_application(app: str, response):
    result = [a.get("name")
              for a in config.APPLICATIONS if a.get("name") == app]
    if len(result) == 0:
        response.status = falcon.HTTP_404
        return {"message": "app not found"}
    return result[0]


@hug.get('/api/{app}/pull')
def pull_single_application(app: str, response):
    result = [a
              for a in config.APPLICATIONS if a.get("name") == app]
    if len(result) == 0:
        response.status = falcon.HTTP_404
        return
    script = result[0].get("update_script")
    if not script:
        response.status = falcon.HTTP_INTERNAL_SERVER_ERROR
        return {"message": "script not found"}
    try:
        process = subprocess.Popen([script], stdout=subprocess.PIPE)
        process.wait()
    except Exception as ex:
        log.error(ex)
        response.status = falcon.HTTP_INTERNAL_SERVER_ERROR
        return {"message": "script went south"}
    return_code = process.returncode
    if return_code == 0:
        response.status = falcon.HTTP_ACCEPTED
        return {"message": "OK", "stdout": process.communicate()[0], "returncode": return_code}
    response.status = falcon.HTTP_INTERNAL_SERVER_ERROR
    return {"message": "Error", "stdout": process.communicate()[0], "returncode": return_code}
