<!-- # SPDX-License-Identifier: MIT -->

# Management console API

## Installation

```bash
python3 -m venv .venv
source .venv/bin/activate
pip install -r requirements.txt
```

## Configuration

```bash
cp config_sample.py config.py
```

Edit

```python
APPLICATIONS = [{
    "name": "$NAME",
    "repository_url": "$URL",
    "access_token": "$TOKEN",
    "update_script": "$PATH_TO_SCRIPT",
    "ping": "$PATH_TO_APP"
}]
```

## Prepare frontend

```
cd client
npm install
```

To test the frontend use `npm run serve` to start the frontend.
To build the frontend use `npm run build` and deploy the dist folder.


## Run

```bash
./run-app.sh
```

Alternatively

```bash
start WSGI-server with server:__hug_wsgi__
```
