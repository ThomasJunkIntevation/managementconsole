# SPDX-License-Identifier: MIT

APPLICATIONS = [{
    "name": "$NAME",
    "repository_url": "$URL",
    "access_token": "$TOKEN",
    "update_script": "$PATH_TO_SCRIPT",
    "ping": "$PATH_TO_APP"
}]
